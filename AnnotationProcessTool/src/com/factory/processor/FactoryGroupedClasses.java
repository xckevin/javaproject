package com.factory.processor;

import java.io.IOException;
import java.io.Writer;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.processing.Filer;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.tools.JavaFileObject;

import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

public class FactoryGroupedClasses {

	private static final String SUFFIX = "Factory";

	private String qualifiedClassName;

	private Map<String, FactoryAnnotatedClass> itemsMap = new LinkedHashMap<String, FactoryAnnotatedClass>();

	public FactoryGroupedClasses(String qualifiedClassName) {
		this.qualifiedClassName = qualifiedClassName;
	}


	public void add(FactoryAnnotatedClass toIntert) throws IdAlreadyUsedExcpetion {
		if(itemsMap.containsKey(toIntert.getId())) {
			throw new IdAlreadyUsedExcpetion(toIntert);
		}
		itemsMap.put(toIntert.getId(), toIntert);
	}

	public void generateCode(Elements elementUtils, Filer filer) throws IOException {
		TypeElement superClassName = elementUtils.getTypeElement(qualifiedClassName);
		String factoryClassName = superClassName.getSimpleName() + SUFFIX;

		JavaFileObject jfo = filer.createSourceFile(qualifiedClassName + SUFFIX);
		Writer writer = jfo.openWriter();

		// use java poet

		MethodSpec.Builder methodSpecBuilder = MethodSpec.methodBuilder("create")
				.addModifiers(Modifier.PUBLIC)
				.returns(TypeName.get(superClassName.asType()))
				.addParameter(String.class, "id")
				.beginControlFlow("if (id == null)")
				.addStatement("throw new IllegalArgumentException(\"id is null!\")")
				.endControlFlow();
		for (FactoryAnnotatedClass item : itemsMap.values()) {
			methodSpecBuilder.beginControlFlow("if ($S.equals(id))", item.getId())
			.addStatement("return new $T()", item.getTypeElement())
			.endControlFlow();
		}
		MethodSpec methodSpec = methodSpecBuilder.addStatement("throw new IllegalArgumentException(\"unkown id = \" + id)")
				.build();

		TypeSpec typeSpec = TypeSpec.classBuilder(factoryClassName)
				.addModifiers(Modifier.PUBLIC)
				.addMethod(methodSpec)
				.build();

		JavaFile factoryFile = null;
		PackageElement pkg = elementUtils.getPackageOf(superClassName);
		if(pkg.isUnnamed()) {
			factoryFile = JavaFile.builder("", typeSpec).build();
		} else {
			factoryFile = JavaFile.builder(pkg.getQualifiedName().toString(), typeSpec).build();
		}
		factoryFile.writeTo(writer);

		writer.close();
	}

}
