package com.factory.processor;

import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.MirroredTypeException;

import com.factory.annotation.Factory;

public class FactoryAnnotatedClass {

	private TypeElement annoatatedElement;
	private String qualifiedSuperClassName;
	private String simpleFactoryGroupName;
	private String id;
	
	public FactoryAnnotatedClass(TypeElement classElements) {
		this.annoatatedElement = classElements;
		Factory annotation = classElements.getAnnotation(Factory.class);
		id = annotation.id();
		
		if(id == null || id.trim().isEmpty()) {
			throw new IllegalArgumentException(String.format("id() in %s for class %s is null or empty! That is not allowed", 
					Factory.class.getSimpleName(), classElements.getQualifiedName().toString()));
		}
		
		try {
			Class<?> clazz = annotation.type();
			qualifiedSuperClassName = clazz.getCanonicalName();
			simpleFactoryGroupName = clazz.getSimpleName();
		} catch(MirroredTypeException e) {
			DeclaredType classTypeMirror = (DeclaredType) e.getTypeMirror();
			TypeElement classTypeElement = (TypeElement) classTypeMirror.asElement();
			qualifiedSuperClassName = classTypeElement.getQualifiedName().toString();
			simpleFactoryGroupName = classTypeElement.getSimpleName().toString();
		}
	}
	public TypeElement getTypeElement() {
		return annoatatedElement;
	}
	public String getQualifiedFactoryGroupName() {
		return qualifiedSuperClassName;
	}
	public String getSimpleFactoryGroupName() {
		return simpleFactoryGroupName;
	}
	public String getId() {
		return id;
	}
}
