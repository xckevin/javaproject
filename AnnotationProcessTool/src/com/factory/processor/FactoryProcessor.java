package com.factory.processor;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic.Kind;

import com.factory.annotation.Factory;
import com.google.auto.service.AutoService;

@AutoService(Processor.class)
public class FactoryProcessor extends AbstractProcessor {

	private Types typeUtils;
	private Elements elementUtils;
	private Filer filer;
	private Messager messager;

	private Map<String, FactoryGroupedClasses> factoryClasses = new LinkedHashMap<String, FactoryGroupedClasses>();

	@Override
	public Set<String> getSupportedAnnotationTypes() {
		Set<String> typs = new LinkedHashSet<String>();
		typs.add(Factory.class.getCanonicalName());
		return typs;
	}

	@Override
	public SourceVersion getSupportedSourceVersion() {
		return SourceVersion.latestSupported();
	}

	@Override
	public synchronized void init(ProcessingEnvironment processingEnv) {
		super.init(processingEnv);
		typeUtils = processingEnv.getTypeUtils();
		elementUtils = processingEnv.getElementUtils();
		filer = processingEnv.getFiler();
		messager = processingEnv.getMessager();
	}

	@Override
	public boolean process(Set<? extends TypeElement> annotations,
			RoundEnvironment roundEnv) {
		for(Element annotatedElement : roundEnv.getElementsAnnotatedWith(Factory.class)) {
			if(annotatedElement.getKind() != ElementKind.CLASS) {
				error(annotatedElement, "Only classes can be annoatated with %s", Factory.class.getSimpleName());
				return true; // exit processing
			}

			System.out.println("start process " + annotatedElement);
			
			TypeElement typeElement = (TypeElement) annotatedElement;
			try {
				FactoryAnnotatedClass annotatedClass = new FactoryAnnotatedClass(typeElement);
				if(!isValidClass(annotatedClass)) {
					return true; // error message printed, exit processing
				}

				String key = annotatedClass.getQualifiedFactoryGroupName();
				System.out.println("annotatedClass key is: " + key);
				FactoryGroupedClasses factoryClass = factoryClasses.get(key);
				if(factoryClass == null) {
					factoryClass = new FactoryGroupedClasses(key);
					factoryClasses.put(key, factoryClass);
				}
				
				factoryClass.add(annotatedClass);
			} catch(IllegalArgumentException e) {
				// factory.id() is empty
				error(typeElement, e.getMessage());
				return true;
			} catch (IdAlreadyUsedExcpetion e) {
				// TODO Auto-generated catch block
				FactoryAnnotatedClass existign = e.getExisting();
				// already existing
				error(annotatedElement,
						"Conflict: The class %s is annotated with %s with id = %s but %s alredy uses the same id",
						typeElement.getQualifiedName().toString(), Factory.class.getSimpleName(),
						existign.getTypeElement().getQualifiedName().toString());
				return true;
			}
			
		}
		
		// generate factory
		try {
			for (FactoryGroupedClasses factoryClass : factoryClasses.values()) {
				factoryClass.generateCode(elementUtils, filer);
			}
			
			factoryClasses.clear();
		} catch (IOException e) {
			error(null, e.getMessage());
		}
		return true;
	}

	private boolean isValidClass(FactoryAnnotatedClass item) {

		// Cast to TypeElement, has more type specific methods
		TypeElement classElement = item.getTypeElement();

		if (!classElement.getModifiers().contains(Modifier.PUBLIC)) {
			error(classElement, "The class %s is not public.",
					classElement.getQualifiedName().toString());
			return false;
		}

		// Check if it's an abstract class
		if (classElement.getModifiers().contains(Modifier.ABSTRACT)) {
			error(classElement, "The class %s is abstract. You can't annotate abstract classes with @%",
					classElement.getQualifiedName().toString(), Factory.class.getSimpleName());
			return false;
		}

		// Check inheritance: Class must be childclass as specified in @Factory.type();
		TypeElement superClassElement =
				elementUtils.getTypeElement(item.getQualifiedFactoryGroupName());
		if (superClassElement.getKind() == ElementKind.INTERFACE) {
			// Check interface implemented
			if (!classElement.getInterfaces().contains(superClassElement.asType())) {
				error(classElement, "The class %s annotated with @%s must implement the interface %s",
						classElement.getQualifiedName().toString(), Factory.class.getSimpleName(),
						item.getQualifiedFactoryGroupName());
				return false;
			}
		} else {
			// Check subclassing
			TypeElement currentClass = classElement;
			while (true) {
				TypeMirror superClassType = currentClass.getSuperclass();

				if (superClassType.getKind() == TypeKind.NONE) {
					// Basis class (java.lang.Object) reached, so exit
					error(classElement, "The class %s annotated with @%s must inherit from %s",
							classElement.getQualifiedName().toString(), Factory.class.getSimpleName(),
							item.getQualifiedFactoryGroupName());
					return false;
				}

				if (superClassType.toString().equals(item.getQualifiedFactoryGroupName())) {
					// Required super class found
					break;
				}

				// Moving up in inheritance tree
				currentClass = (TypeElement) typeUtils.asElement(superClassType);
			}
		}

		// Check if an empty public constructor is given
		for (Element enclosed : classElement.getEnclosedElements()) {
			if (enclosed.getKind() == ElementKind.CONSTRUCTOR) {
				ExecutableElement constructorElement = (ExecutableElement) enclosed;
				if (constructorElement.getParameters().size() == 0 && constructorElement.getModifiers()
						.contains(Modifier.PUBLIC)) {
					// Found an empty constructor
					return true;
				}
			}
		}

		// No empty constructor found
		error(classElement, "The class %s must provide an public empty default constructor",
				classElement.getQualifiedName().toString());
		return false;
	}

	private void error(Element e, String msg, Object...args) {
		messager.printMessage(Kind.ERROR, String.format(msg, args), e);
	}

}
