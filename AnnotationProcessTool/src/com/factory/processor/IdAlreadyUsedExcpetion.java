package com.factory.processor;

import com.factory.processor.FactoryAnnotatedClass;

public class IdAlreadyUsedExcpetion extends Exception {
	
	private FactoryAnnotatedClass existing;

	public IdAlreadyUsedExcpetion() {
		super();
		// TODO Auto-generated constructor stub
	}

	public IdAlreadyUsedExcpetion(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public IdAlreadyUsedExcpetion(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public IdAlreadyUsedExcpetion(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public IdAlreadyUsedExcpetion(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public IdAlreadyUsedExcpetion(FactoryAnnotatedClass toIntert) {
		super();
		this.existing = toIntert;
		
	}

	public FactoryAnnotatedClass getExisting() {
		return existing;
	}

}
