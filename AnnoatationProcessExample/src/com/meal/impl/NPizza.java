package com.meal.impl;

import com.factory.annotation.Factory;
import com.meal.Meal;

@Factory(id="N", type=Meal.class)
public class NPizza implements Meal {

	@Override
	public float getPrice() {
		return 5.6f;
	}

}
