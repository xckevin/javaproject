package com.meal.impl;

import com.factory.annotation.Factory;
import com.meal.Meal;

@Factory(id="C", type = Meal.class)
public class CPizza implements Meal {

	@Override
	public float getPrice() {
		return 8.5f;
	}

}
