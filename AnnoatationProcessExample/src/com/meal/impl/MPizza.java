package com.meal.impl;

import com.factory.annotation.Factory;
import com.meal.Meal;

@Factory(id="M", type=Meal.class)
public class MPizza implements Meal {

	@Override
	public float getPrice() {
		return 5f;
	}

}
