package com.store;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;

import com.meal.Meal;

public class PizzaStore {
	
	private Object obj;
	
	private Method method;
	
	public PizzaStore() throws Exception {
		Class<?> clazz = Class.forName("com.meal.MealFactory");
		obj = clazz.newInstance();
		method = clazz.getMethod("create", String.class);
	}

	public Meal order(String mealName) throws Exception {
		return (Meal) method.invoke(obj, mealName);
	}
	
	public static void main(String[] args) throws Exception {
		PizzaStore store = new PizzaStore();
		Meal meal = store.order(readConsole());
		System.out.println("Bill: $" + meal.getPrice());
	}

	private static String readConsole() throws IOException {
		System.out.println("What do you like?");
	    BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
	    String input = bufferRead.readLine();
	    return input;
	}
	
	
}
